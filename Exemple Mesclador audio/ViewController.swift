//
//  ViewController.swift (Swift 3).
//  Ejemplo Mezclador Audio simple.
//
//  Created by Francesc Roig.
//  Revisión: 22/4/2018.
//

import UIKit
import AVFoundation // Necesario para la reproducción de audio/video. 


/**
 * Bloque principal ViewController
 */
class ViewController: UIViewController {

    // Componentes reproductores de audio.
    var player1: AVAudioPlayer!
    var player2: AVAudioPlayer!
    
    // Controles de volumen
    @IBOutlet weak var componenteSlider1: UISlider!
    @IBOutlet weak var componenteSlider2: UISlider!
    
    
    /*
     * Función que se ejecuta al inicio de la app.
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Provoca la reotación del los Sliders en posición vertical (hace una rotación de -90 grados),
        // para darle un aspecto más realista de mezclador.
        componenteSlider1.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi/2))
        componenteSlider2.transform = CGAffineTransform.init(rotationAngle: CGFloat(-Double.pi/2))
        
        // Carga los audios en los componentes reproductores.
        // (las canciones estan cargadas dentre de 'Assets.xcassets')
        cargaAudio(nombre: "The Crab Apples - Three II", player: &player1)
        cargaAudio(nombre: "The Crab Apples - Right Here", player: &player2)
    }


    
    /*
     * Acción boton "Reproduce canción 1".
     */
    @IBAction func accionBotonReproduce1(_ sender: Any) {
        
        player1.play()
    }

    
    
    /*
     * Acción boton "Reproduce canción 2".
     */
    @IBAction func accionBotonReproduce2(_ sender: Any) {
        
        player2.play()
    }
    
    
    
    /*
     * Acción boton "Stop canción 1".
     */
    @IBAction func accionBotonStop1(_ sender: Any) {
        
        player1.stop()
        player1.currentTime = 0
    }
    
    
    
    /*
     * Acción boton "Stop canción 2".
     */
    @IBAction func accionBotonStop2(_ sender: Any) {
        
        player2.stop()
        player2.currentTime = 0
    }
    
    
    
    /*
     * Acción de respuesta al desplazamiento del Slider 1 (Volumen audio 1).
     */
    @IBAction func accionSliderVolumen1(_ sender: Any) {
        
        player1.setVolume(componenteSlider1.value, fadeDuration: 0)
    }
    
    
    
    /*
     * Acción de respuesta al desplazamiento del Slider 2 (Volumen audio 2).
     */
    @IBAction func accionSliderVolumen2(_ sender: Any) {
        
         player2.setVolume(componenteSlider2.value, fadeDuration: 0)
    }
    
    
    
    /*
     * Función que carga el audio seleccionado en 'nombre' a un componente reproductor en 'player'.
     */
    func cargaAudio(nombre:String, player: inout AVAudioPlayer!)  {
        
        guard let sound = NSDataAsset(name: nombre) else {
            print("Audio no encontrado!!")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(data: sound.data)
        } catch let error as NSError {
            print("Error de reproducción: \(error.localizedDescription)")
        }
    }
    
} // Final bloque ViewController

